package com.devcamp.s10.jbr310.restapi;


import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RainbowRestAPI {
    @CrossOrigin
    @GetMapping("/rainbow")
    public ArrayList<String> rainbow() {
        ArrayList<String> rainbow = new ArrayList<>();
        rainbow.add("red");
        rainbow.add("orange");
        rainbow.add("yellow");
        rainbow.add("green");
        rainbow.add("blue");

        return rainbow;
    }
}
